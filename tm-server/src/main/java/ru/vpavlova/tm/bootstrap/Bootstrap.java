package ru.vpavlova.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.api.IPropertyService;
import ru.vpavlova.tm.api.service.IActiveMQConnectionService;
import ru.vpavlova.tm.api.service.ILoggerService;
import ru.vpavlova.tm.api.service.model.IUserGraphService;
import ru.vpavlova.tm.component.Backup;
import ru.vpavlova.tm.endpoint.AbstractEndpoint;
import ru.vpavlova.tm.enumerated.Role;
import ru.vpavlova.tm.service.ActiveMQConnectionService;
import ru.vpavlova.tm.service.LoggerService;
import ru.vpavlova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Setter
@Component
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    @Autowired
    AbstractEndpoint[] abstractEndpoints;

    @NotNull
    public ILoggerService loggerService  = new LoggerService();;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    @Autowired
    public IUserGraphService userService;

    @Autowired
    private Backup backup;

    @NotNull
    public IActiveMQConnectionService activeMQConnectionService;

    private void textWelcome() {
        loggerService.debug("TEST!!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
    }

    private void initActiveMQConnectionService() {
        activeMQConnectionService = new ActiveMQConnectionService();
    }

    @SneakyThrows
    public void initJMSBroker() {
        @NotNull final BrokerService broker = new BrokerService();
        @NotNull final String bindAddress = "tcp://" + BrokerService.DEFAULT_BROKER_NAME + ":" + BrokerService.DEFAULT_PORT;
        broker.addConnector(bindAddress);
        broker.start();
    }

    private void initEndpoint() {
        Arrays.stream(abstractEndpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void initUser() {
        userService.create("test", "test");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    public void init(@Nullable final String... args) {
        textWelcome();
        initPID();
        initJMSBroker();
        initActiveMQConnectionService();
        initEndpoint();
        initUser();
        backup.init();
    }

}

package ru.vpavlova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.vpavlova.tm.api.repository.IGraphRepository;
import ru.vpavlova.tm.entity.UserGraph;

import java.util.Optional;

public interface IUserGraphRepository extends IGraphRepository<UserGraph> {

    @NotNull
    Optional<UserGraph> findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

}
package ru.vpavlova.tm.repository.model;

import ru.vpavlova.tm.api.repository.IGraphRepository;
import ru.vpavlova.tm.entity.SessionGraph;

public interface ISessionGraphRepository extends IGraphRepository<SessionGraph> {

}


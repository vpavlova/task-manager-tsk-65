package ru.vpavlova.tm;

import org.hsqldb.server.ServerConfiguration;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vpavlova.tm.bootstrap.Bootstrap;
import ru.vpavlova.tm.util.SystemUtil;

public class Application {

    public static void main(final String[] args) {
        System.out.println("PID: " + SystemUtil.getPID());
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init(args);
    }

}

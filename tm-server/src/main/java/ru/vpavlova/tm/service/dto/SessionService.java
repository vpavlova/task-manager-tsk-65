package ru.vpavlova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.api.IPropertyService;
import ru.vpavlova.tm.repository.dto.ISessionRepository;
import ru.vpavlova.tm.api.service.dto.ISessionService;
import ru.vpavlova.tm.api.service.dto.IUserService;
import ru.vpavlova.tm.dto.Session;
import ru.vpavlova.tm.dto.User;
import ru.vpavlova.tm.enumerated.Role;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.user.AccessDeniedException;
import ru.vpavlova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

@Service
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    public ISessionRepository sessionRepository;

    @NotNull
    public ISessionRepository getRepository() {
        return sessionRepository;
    }

    @Autowired
    private IPropertyService propertyService;

    @Autowired
    private IUserService userService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final Session session) {
        if (session == null) throw new ObjectNotFoundException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.save(session);
    }

    @SneakyThrows
    @Transactional
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login.isEmpty() || password.isEmpty()) return false;
        @NotNull final Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) return false;
        if (user.get().isLocked()) throw new AccessDeniedException();
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.get().getPasswordHash());
    }

    @Nullable
    @Transactional
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.get().getId());
        @Nullable final Session signSession = sign(session);
        if (signSession == null) return null;
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.save(signSession);
        return signSession;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if ((session.getSignature().isEmpty())) throw new AccessDeniedException();
        if ((session.getUserId()).isEmpty()) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        @NotNull final ISessionRepository sessionRepository = getRepository();
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.existsById(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void validateAdmin(@Nullable final Session session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (session.getUserId().isEmpty()) throw new AccessDeniedException();
        validate(session);
        @NotNull final Optional<User> user = userService.findOneById(session.getUserId());
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public Session close(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.deleteById(session.getId());
        return session;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Session> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        entities.forEach(sessionRepository::save);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Session entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.deleteById(entity.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Session> findAll() {
        @NotNull final ISessionRepository sessionRepository = getRepository();
        return sessionRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Optional<Session> findOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        return Optional.of(sessionRepository.getOne(id));
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.deleteById(id);
    }

}


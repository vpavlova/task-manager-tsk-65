package ru.vpavlova.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.service.SessionService;

public abstract class AbstractListener {

    @Nullable
    @Autowired
    protected SessionService sessionService;

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    @Nullable
    public abstract String name();

    public abstract void handler(@NotNull ConsoleEvent event);

    @NotNull
    @Override
    public String toString() {
        String result = "";
        if (!name().isEmpty()) { result += name(); }
        if (!arg().isEmpty()) result += " [" + arg() + "]";
        if (!description().isEmpty()) result += " - " + description();
        return result;
    }

}


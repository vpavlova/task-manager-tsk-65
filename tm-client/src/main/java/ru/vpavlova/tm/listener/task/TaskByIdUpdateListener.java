package ru.vpavlova.tm.listener.task;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractTaskListener;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.endpoint.Task;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.TaskNotFoundException;
import ru.vpavlova.tm.util.TerminalUtil;

import java.util.Optional;


@Component
public class TaskByIdUpdateListener extends AbstractTaskListener {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-status-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task status by id.";
    }

    @Override
    @EventListener(condition = "@taskByIdUpdateListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Task task = taskEndpoint.findTaskById(session, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        taskEndpoint.updateTaskById(session, id, name, description);
    }

}

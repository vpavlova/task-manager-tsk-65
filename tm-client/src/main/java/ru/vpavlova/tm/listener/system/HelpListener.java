package ru.vpavlova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractListener;

@Component
public class HelpListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Show terminal commands.";
    }

    @Override
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[HELP]");
        for (@NotNull final AbstractListener listener : listeners) {
            System.out.println(listener);
        }
    }

}
